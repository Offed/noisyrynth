package com.example.pitchmaze.view

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.pitchmaze.model.WorldFieldType
import com.example.pitchmaze.model.system.Vector2i

class FieldView(private val size : Vector2i, val type : WorldFieldType) {
    var position = Vector2i(0, 0)
    private val color = when (type) {
        WorldFieldType.Wall -> Color.BLACK
        WorldFieldType.Player -> Color.GREEN
        WorldFieldType.Empty -> Color.WHITE
        WorldFieldType.Finish -> Color.BLUE
        WorldFieldType.Unknown -> Color.RED
    }

    fun draw (canvas : Canvas) {
        val paint = Paint()

        paint.color = color
        paint.style = Paint.Style.FILL
        paint.strokeWidth = 8f
        paint.isAntiAlias = true

        canvas.drawRect(
            position.x.toFloat(),
            position.y.toFloat(),
            (position.x + size.x).toFloat(),
            (position.y + size.y).toFloat(),
            paint
        )
    }
}