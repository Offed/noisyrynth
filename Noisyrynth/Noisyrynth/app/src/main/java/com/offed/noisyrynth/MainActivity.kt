package com.offed.noisyrynth

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.io.File
import java.io.IOException

class MainActivity : AppCompatActivity() {

    val LOG_TAG = "MainActivity"
    var REQUEST_RECORD_AUDIO_PERMISSION = 200

    //requesting permission for audio rec
    private var permissionToRecordAccepted = false
    private val permissions =
        arrayOf(Manifest.permission.RECORD_AUDIO)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_RECORD_AUDIO_PERMISSION -> permissionToRecordAccepted =
                grantResults[0] == PackageManager.PERMISSION_GRANTED
        }
        if (!permissionToRecordAccepted) finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ActivityCompat.requestPermissions(
            this,
            permissions,
            REQUEST_RECORD_AUDIO_PERMISSION
        )


        if (!isExternalStorageManagable()) {
            Log.e(LOG_TAG, "External strorage is not managable.")
            finish()
        }

        if (!ExternalStorageStructurePresent()) {
            try {
                UtilityClass.index =
                    UtilityClass.deviceIndex(this, UtilityClass.structureName!!, true)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            setupExternalStorageStructure()
            try {
                UtilityClass.index =
                    UtilityClass.deviceIndex(this, UtilityClass.structureName!!, false)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            try {
                UtilityClass.index =
                    UtilityClass.deviceIndex(this, UtilityClass.structureName!!, false)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


    }

    private fun isExternalStorageManagable(): Boolean {
        Log.i(LOG_TAG, "Checking read/write in external storage")
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }

    private fun ExternalStorageStructurePresent(): Boolean {
        UtilityClass.structureName = getExternalFilesDir(null).toString() + "/devices"
        Log.i(LOG_TAG, "Expected dir is " + UtilityClass.structureName)
        val f = File(UtilityClass.structureName)
        //return f.isDirectory
        return false
    }

    private fun setupExternalStorageStructure() {
        Log.i(LOG_TAG, "Setting up app structure")
        val structureName = getExternalFilesDir(null).toString() + "/devices"
        val devicesDir = File(structureName)
        devicesDir.mkdirs()
        UtilityClass.copyResDir(this, structureName)
    }


    fun openSelectDevice(view: View?) {
        val intent = Intent(this, selectDeviceActivity::class.java)
        startActivity(intent)
    }
}
