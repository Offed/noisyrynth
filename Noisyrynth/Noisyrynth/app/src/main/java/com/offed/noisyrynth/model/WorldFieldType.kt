package com.example.pitchmaze.model

enum class WorldFieldType {
    Wall,
    Player,
    Empty,
    Finish,
    Unknown
}