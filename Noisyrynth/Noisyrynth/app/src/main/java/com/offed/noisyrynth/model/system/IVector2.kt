package com.example.pitchmaze.model.system

interface IVector2<T> {
    var x : T
    var y : T
}