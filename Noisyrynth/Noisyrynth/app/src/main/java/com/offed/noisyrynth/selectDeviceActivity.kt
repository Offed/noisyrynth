package com.offed.noisyrynth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.offed.noisyrynth.pitchDetectionActivity

class selectDeviceActivity : AppCompatActivity() {
    var deviceList: ListView? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_device)
        val deviceSet: Set<String?> =
            UtilityClass.index!!.availableDevices.keys
        val devices = deviceSet.toTypedArray()
        val adapter: ArrayAdapter<Any?> = ArrayAdapter<Any?>(
            this,
            R.layout.device_list_element,  //list elements (textViews)
            devices
        ) //items to populate the list
        deviceList = findViewById(R.id.deviceList) as ListView?
        deviceList!!.adapter = adapter
        deviceList!!.onItemClickListener = OnItemClickListener { adapterView, view, i, l ->
            val fancyDeviceName =
                deviceList!!.getItemAtPosition(i) as String
            Log.i("ITEM_CHOSEN", "Chosen device $fancyDeviceName")
            val fileName = UtilityClass.deviceIndex.getFilename(fancyDeviceName)
            val intent =
                Intent(applicationContext, pitchMazeActivity::class.java)
            intent.putExtra("deviceFileName", fileName)
            startActivity(intent)
        }
    }
}