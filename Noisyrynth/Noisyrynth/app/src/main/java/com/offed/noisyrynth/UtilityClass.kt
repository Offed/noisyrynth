package com.offed.noisyrynth

import android.content.Context
import android.media.AudioFormat
import android.media.AudioRecord
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Offed on 2017-09-26.
 */
object UtilityClass {
    var LOG_TAG = "UTILITY_CLASS"
    var SOUND_IDENTIFY_THRESHOLD = 2
    var SAME_SOUND_THRESHOLD = 5 //TODO: add a setting to set these up
    var MIN_DETECTION_INTERVAL_BETWEEN: Long = 200
    var index: deviceIndex? = null
    var structureName: String? = null
    val timestampString: String
        get() {
            val currentTime = Calendar.getInstance()
            val dateFormat = SimpleDateFormat("dd-MMM-yyyy_HH-mm")
            return dateFormat.format(currentTime.time)
        }

    // add the rates you wish to check against
    val maximumSupportedSampleRate: Int
        get() {
            var maximumSampleRate = 0
            for (rate in intArrayOf(22050, 44100)) { // add the rates you wish to check against
                val bufferSize = AudioRecord.getMinBufferSize(
                    rate,
                    AudioFormat.CHANNEL_CONFIGURATION_DEFAULT,
                    AudioFormat.ENCODING_PCM_16BIT
                )
                if (bufferSize > 0 && rate > maximumSampleRate) {
                    maximumSampleRate = rate
                }
            }
            return maximumSampleRate
        }

    fun copyResDir(ctx: Context, structureName: String) {
        val filenames: Set<String> =
            index!!.availableDevices.keys
        for (filename in filenames) {
            copyFile(ctx, filename, structureName)
        }
        copyFile(ctx, "index", structureName)
    }

    private fun copyFile(
        ctx: Context,
        filename: String,
        structureName: String
    ) {
        var filename: String? = filename
        if (filename !== "index") filename = deviceIndex.getFilename(filename)
        val iStream = ctx.resources.openRawResource(
            ctx.resources.getIdentifier(
                filename,
                "raw", ctx.packageName
            )
        )
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream("$structureName/$filename")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        val buff = ByteArray(1024)
        var read = 0
        try {
            while (iStream.read(buff).also { read = it } > 0) {
                out!!.write(buff, 0, read)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                iStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            try {
                out!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    class deviceIndex internal constructor(
        ctx: Context,
        structureName: String,
        firstRun: Boolean
    ) {
        var availableDevices: MutableMap<String, String> =
            HashMap()

        companion object {
            fun getFilename(fancyName: String?): String? {
                return index!!.availableDevices[fancyName]
            }
        }

        /*
        * Function used to read index of available devices from raw folder
         * and construct an easy-to-use structure
        * */
        init {
            var iStream: InputStream? = null
            iStream = if (firstRun) {
                ctx.resources.openRawResource(R.raw.index)
            } else {
                Log.i(
                    LOG_TAG,
                    "Expected index file at $structureName/index"
                )
                val file = File("$structureName/index")
                if (file.exists()) {
                    Log.i(LOG_TAG, "Index.json OK")
                }
                FileInputStream(file)
            }
            val writer: Writer = StringWriter()
            val buffer = CharArray(iStream.available())
            try {
                val reader: Reader =
                    BufferedReader(InputStreamReader(iStream, "UTF-8"))
                var n: Int
                while (reader.read(buffer).also { n = it } != -1) {
                    writer.write(buffer, 0, n)
                }
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                iStream.close()
            }
            val indexString = String(buffer)
            var deviceEntries: JSONArray? = null
            try {
                deviceEntries = JSONArray(indexString)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            try {
                for (i in 0 until deviceEntries!!.length()) {
                    val jsonDeviceEntry = deviceEntries.getJSONObject(i)
                    val deviceFancyName = jsonDeviceEntry.getString("fancyName")
                    val deviceFileName = jsonDeviceEntry.getString("fileName")
                    availableDevices[deviceFancyName] = deviceFileName
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }
}