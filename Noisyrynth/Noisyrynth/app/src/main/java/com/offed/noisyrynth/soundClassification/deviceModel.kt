package com.offed.noisyrynth.soundClassification

import com.example.pitchmaze.model.Direction
import com.offed.noisyrynth.UtilityClass
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by Offed on 2017-10-08.
 */
class deviceModel(jsonObject: JSONObject) {
    val keySounds: ArrayList<keySound> = ArrayList<keySound>()
    var deviceName: String? = null
        private set
    private var lastProcessedPitch = 0f
    private var lastDetectedKey: keySound? = null
    private var timeLastDetected: Long = 0
    val gameMap = mutableListOf<MutableList<Char>>()

    private fun setDeviceName(deviceName: String) {
        this.deviceName = deviceName
    }

    private fun addKeySound(key: keySound) {
        keySounds.add(key)
    }

    private fun updateDetectionState(time: Long, key: keySound) {
        lastDetectedKey = key
        timeLastDetected = time
    }

    fun matchFrequencyToKey(detectedFrequency: Float): keySound? { //reduce number of false sound detections
        if (Math.abs(lastProcessedPitch - detectedFrequency) < UtilityClass.SAME_SOUND_THRESHOLD) {
            return null
        }
        lastProcessedPitch = detectedFrequency
        //for each detected sound try to match it with defined sounds
        for (key in keySounds) {
            val candidateFrequency: Int = key.soundFrequency
            val distanceBetween =
                Math.abs(candidateFrequency - detectedFrequency)
            //if sound classifies as valid key
            if (distanceBetween < UtilityClass.SOUND_IDENTIFY_THRESHOLD) { //if it's the first sound detected
                if (lastDetectedKey == null) {
                    updateDetectionState(System.currentTimeMillis(), key)
                    return key
                }
                //if current detected key is the same as previous one
                if (lastDetectedKey!!.equals(key)) {
                    val currentTime = System.currentTimeMillis()
                    //if appropriate amount of time passed since last key detected
                    return if (currentTime - timeLastDetected > UtilityClass.MIN_DETECTION_INTERVAL_BETWEEN) {
                        updateDetectionState(currentTime, key)
                        key
                    } else { //if interval between same key detections was too short
                        timeLastDetected = currentTime
                        null
                    }
                }
                //if previous sound was different than current one
                updateDetectionState(System.currentTimeMillis(), key)
                return key
            }
        }
        //if sound not found among candidates return null
        return null
    }

    init {
        try { //set name from JSON
            val jsonDeviceName = jsonObject.getString("deviceName")
            setDeviceName(jsonDeviceName)
            //read sound signatures from JSON
            val jsonKeySounds = jsonObject.getJSONArray("keySounds")
            for (i in 0 until jsonKeySounds.length()) {
                val jsonKeySound = jsonKeySounds.getJSONObject(i)
                val jsonSoundName = jsonKeySound.getString("soundName")
                val jsonSoundFrequency = jsonKeySound.getInt("soundFrequency")
                val direction = when(jsonKeySound.getInt("direction")){
                    0 -> Direction.Top
                    1 -> Direction.Down
                    2 -> Direction.Left
                    3 -> Direction.Right
                    else -> Direction.Top
                }
                addKeySound(keySound(jsonSoundName, direction, jsonSoundFrequency))
            }
            val jsonGameMap = jsonObject.getString("gameMap")

            for (line in jsonGameMap.split("\n")){
                var lineArray = line.toCharArray().toMutableList()
                gameMap.add(lineArray)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}