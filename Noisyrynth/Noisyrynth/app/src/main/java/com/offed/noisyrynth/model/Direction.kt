package com.example.pitchmaze.model

enum class Direction {
    Top,
    Down,
    Left,
    Right
}