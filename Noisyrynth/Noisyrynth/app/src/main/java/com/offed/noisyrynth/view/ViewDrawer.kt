package com.example.pitchmaze.view

import android.graphics.*
import android.widget.ImageView
import com.example.pitchmaze.model.IWorld
import com.example.pitchmaze.model.WorldFieldType
import com.example.pitchmaze.model.system.Vector2i
import java.util.logging.Logger

class ViewDrawer {
    lateinit var view : ImageView
    val paint = Paint()

    fun drawWorld(world : IWorld) {
        val worldSize = world.worldSize
        val canvasSize = Vector2i(500, 500)
        val fieldSize = (canvasSize / worldSize) as Vector2i

        val fieldList = listOf(
            FieldView(fieldSize, WorldFieldType.Player),
            FieldView(fieldSize, WorldFieldType.Wall),
            FieldView(fieldSize, WorldFieldType.Empty),
            FieldView(fieldSize, WorldFieldType.Finish),
            FieldView(fieldSize, WorldFieldType.Unknown)
        )

        if (worldSize.x <= 0 || worldSize.y <= 0) {
            println("Cannot draw world with size: ${worldSize.x}x${worldSize.y}")
            return
        }

        val bitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        for (y in 0 until worldSize.x) {
            for(x in 0 until worldSize.y) {
                fieldList.first{
                    it.position = (Vector2i(x, y) * fieldSize) as Vector2i
                    it.type == world.getStateAt(Vector2i(x, y))
                }.draw(canvas)
            }
        }

//        world.getStateAt(Vector2i(3 , 3))
        view.setImageBitmap(bitmap)
    }

    companion object {
        const val defaultColor = Color.WHITE;
    }
}