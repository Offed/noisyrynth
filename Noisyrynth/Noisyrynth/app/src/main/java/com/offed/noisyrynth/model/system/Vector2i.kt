package com.example.pitchmaze.model.system

data class Vector2i(override var x : Int, override var y : Int) : IVector2<Int> {
    operator fun plus (other : IVector2<Int>) : IVector2<Int> {
        return Vector2i(x + other.x, y + other.y)
    }
    operator fun minus (other : IVector2<Int>) : IVector2<Int> {
        return Vector2i(x - other.x, y - other.y)
    }
    operator fun times (other : IVector2<Int>) : IVector2<Int> {
        return Vector2i(x * other.x, y * other.y)
    }
    operator fun div (other : IVector2<Int>) : IVector2<Int> {
        return Vector2i(x / other.x, y / other.y)
    }
}