package com.example.pitchmaze.model.extensions

import com.example.pitchmaze.model.system.IVector2
import com.example.pitchmaze.model.system.Vector2i

fun MutableList<MutableList<Char>>.at(position : IVector2<Int>) : Char {
    return get(position.y).get(position.x)
}


fun MutableList<MutableList<Char>>.removeAtInnerList(position : IVector2<Int>) {
    get(position.y).removeAt(position.x)
}

fun MutableList<MutableList<Char>>.addAtInnerList(position : IVector2<Int>, element : Char) {
    get(position.y).add(position.x, element)
}

fun MutableList<MutableList<Char>>.dimensions() : IVector2<Int> {
    return Vector2i(
        size,
        maxBy { it.size }?.size ?: 0
    )
}